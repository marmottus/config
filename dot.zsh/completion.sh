#######################
# COMPLETION TWEAKING #
#######################

# The following lines were added by compinstall
_compdir=~/usr/share/zsh/functions
[[ -z $fpath[(r)$_compdir] ]] && fpath=($fpath $_compdir)

autoload -U compinit; compinit
autoload -U colors; colors
autoload -U zsh-mime-setup; zsh-mime-setup
autoload -U zsh-mime-handler

zmodload zsh/complist

zstyle ':mime:*' mailcap /etc/mailcap

# on processes completion complete all user processes
zstyle ':completion:*:processes' command 'ps -au$USER'
# format on completion
# zstyle ':completion:*:descriptions'a format \
#        $'%{\e[0;37m%}completing %B%d%b%{\e[0m%}'
# provide verbose completion information
zstyle ':completion:*' verbose yes
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format \
    $'%{\e[0;31m%}No matches for:%{\e[0m%} %d'
# separate matches into groups
zstyle ':completion:*:matches' group 'yes'
# describe options in full
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
zstyle ':completion:*:*:zcompile:*' ignored-patterns '(*~|*.zwc)'

# activate color-completion(!)
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

## correction

# Ignore completion functions for commands you don't have:
#  zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion::(^approximate*):*:functions' ignored-patterns '_*'

zstyle ':completion:*'             completer _complete _correct _approximate
zstyle ':completion:*:correct:*'   insert-unambiguous true
#  zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
#  zstyle ':completion:*:corrections' format $'%{\e[0;31m%}%d (errors: %e)%}'
zstyle ':completion:*:corrections' \
    format $'%{\e[0;31m%}%d (errors: %e)%{\e[0m%}'
zstyle ':completion:*:correct:*'   original true
zstyle ':completion:correct:'      prompt 'correct to:'

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:' \
    max-errors 'reply=( $((($#PREFIX+$#SUFFIX)/3 )) numeric )'
#  zstyle ':completion:*:correct:*'   max-errors 2 numeric

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# ignore duplicate entries
zstyle ':completion:*:history-words' stop yes
zstyle ':completion:*:history-words' remove-all-dups yes
zstyle ':completion:*:history-words' list false
zstyle ':completion:*:history-words' menu yes

# filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' \
    ignored-patterns  '*?.(o|c~|old|pro|zwc)' '*~'

# Don't complete backup files as executables
zstyle ':completion:*:complete:-command-::commands' ignored-patterns '*\~'

# If there are more than N options, allow selecting from a menu with
# arrows (case insensitive completion!).
#  zstyle ':completion:*-case' menu select=5
zstyle ':completion:*' menu select=2

zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# caching
[ -d $ZSHDIR/cache ] && zstyle ':completion:*' use-cache yes && \
    zstyle ':completion::complete:*' cache-path $ZSHDIR/cache/

known_hosts=''
[ -f "$HOME/.ssh/known_hosts" ] && \
    known_hosts="`awk '$1!~/\|/{print $1}' \
$HOME/.ssh/known_hosts | cut -f1 -d, | xargs`"

zstyle ':completion:*:hosts' hosts ${=known_hosts}

# Debian specific stuff
# zstyle ':completion:*:*:lintian:*' file-patterns '*.deb'
zstyle ':completion:*:*:linda:*'   file-patterns '*.deb'

# see upgrade function in this file
compdef _hosts upgrade
