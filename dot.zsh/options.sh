###############
# ZSH OPTIONS #
###############

setopt correct                  # spelling correction
setopt complete_in_word         # not just at the end
setopt alwaystoend              # when completing within a word,
				# move cursor to the end of the word
setopt auto_cd                  # change to dirs without cd
setopt hist_ignore_all_dups     # If a new command added to the history
				# list duplicates an older one, the older is removed from the list
setopt hist_find_no_dups        # do not display duplicates when searching
				#for history entries
setopt auto_list                # Automatically list choices on
				# an ambiguous completion.
setopt auto_param_keys          # Automatically remove
				# undesirable characters added
				# after auto completions when necessary
setopt auto_param_slash         # Add slashes at the end of
				# auto completed dir names
#setopt no_bg_nice              # ??
setopt complete_aliases
setopt equals                   # If a word begins with an unquoted `=',
				# the remainder of the word is taken as the name of a command.
                                # If a command exists by that name,
				# the word is replaced by the full pathname of the command.
setopt extended_glob            # activates: ^x
				# Matches anything except the pattern x.
                                #            x~y
				# Match anything that matches the pattern x but does not match y.
                                #            x#
				# Matches zero or more occurrences of the pattern x.
                                #            x##
				# Matches one or more occurrences of the pattern x.
setopt hash_cmds                # Note the location of each command the first
				# time it is executed in order to avoid search
				# during subsequent invocations
setopt hash_dirs                # Whenever a command name is hashed,
				# hash the directory containing it
setopt mail_warning             # Print a warning message if a mail
				# file has been accessed since the shell last checked.
setopt append_history           # append history list to the history
				# file (important for multiple parallel zsh sessions!)
#setopt share_history           # imports new commands from the history file,
				# causes your typed commands to be
				# appended to the history file

setopt AUTO_LIST
setopt ALWAYS_TO_END
setopt AUTO_LIST
setopt ALWAYS_LAST_PROMPT
setopt AUTO_MENU
setopt AUTO_PARAM_SLASH
setopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD
setopt HASH_LIST_ALL
setopt LIST_AMBIGUOUS
setopt LIST_TYPES
setopt ALIASES
setopt CORRECT
setopt CORRECT_ALL
setopt INTERACTIVE_COMMENTS
setopt HASH_CMDS
setopt HASH_DIRS
setopt AUTO_CONTINUE
setopt AUTO_RESUME
setopt BG_NICE
setopt LONG_LIST_JOBS
setopt TRANSIENT_RPROMPT
setopt C_BASES
setopt FUNCTION_ARGZERO
setopt BSD_ECHO
setopt EMACS
setopt ZLE
setopt NULLGLOB
setopt AUTO_REMOVE_SLASH
setopt CHASE_LINKS
setopt HIST_VERIFY
setopt AUTO_CD
setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_SILENT
setopt PUSHD_TO_HOME
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS

unsetopt LIST_AMBIGUOUS
unsetopt GLOB_DOTS
unsetopt LIST_BEEP
unsetopt RM_STAR_SILENT
unsetopt HUP
unsetopt MAIL_WARNING
unsetopt HIST_BEEP
unsetopt BEEP
unsetopt ALL_EXPORT
unsetopt GLOBAL_RCS

#autoload mere zed
#autoload zfinit
autoload -U zmv

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn hg
precmd() {
    vcs_info
}

zstyle ':vcs_info:git*' formats "%{$fg_dgray%} > %{$fg_yellow%}%s %{$none%}[%{$fg_lgreen%}%b%{$none%}]%m%u%c"
zstyle ':vcs_info:hg*' formats "%{$fg_dgray%} > %{$fg_yellow%}%s %{$none%}[%{$fg_lgreen%}%b%{$none%}]%m%u%c"
