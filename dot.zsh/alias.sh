###########
# Aliases #
###########

# Images
for e in jpg jpeg git bmp pbm pgm ppm tga xbm xpm tif tiff png; do
    LS_COLORS="$LS_COLORS:*.${e}=01;32"
done

# Archives
for e in tar tgz arj taz lzh zip z Z gz bz2 rar deb rpm jar; do
    LS_COLORS="$LS_COLORS:*.${e}=00;32"
done

# Movies
for e in mov mpg mpeg avi mp4 fli; do
    LS_COLORS="$LS_COLORS:*.${e}=00;31"
done

# Music
for e in ogg mp3 wav; do
    LS_COLORS="$LS_COLORS:*.${e}=01;31"
done


LS_COLORS="$LS_COLORS:no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;31:do=01;31"
LS_COLORS="$LS_COLORS:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;33"

LS_COLORS="$LS_COLORS:*.gl=01;31:*.dl=01;31:*.xcf=01;31:*.xwd=01;31"
LS_COLORS="$LS_COLORS:*.pdf=00;33"

export LS_COLORS
export LS_OPTIONS='-F'

lsbin='ls'
case `uname -s` in
    *BSD | Darwin)
	gls --version >/dev/null 2>/dev/null && lsbin='gls' && \
	    LS_OPTIONS="$LS_OPTIONS -b -h --color"
	;;
    Linux | CYGWIN*)
	LS_OPTIONS="$LS_OPTIONS -b -h --color"
	;;
esac

alias ls="LS_COLORS='$LS_COLORS' $lsbin $LS_OPTIONS"
alias ll='ls -l'
#[ `hostname` != "zoidberg" ] && alias ll='ls++ --potsf'
alias la='ll -a'
alias LS='sl'


alias less='less -R'
alias grep='grep --color=always'
alias du='du -h'
alias git_diff='git diff --cached --name-only'
alias rme='rm -vf *~ \#*\# .*~ .\#*\#'
alias slrn_lrde="NNTPSERVER='snews://news.lrde.epita.fr' \
slrn -f ~/.jnewsrc-lrde"
alias postgres='sudo su postgres'
alias valgrind='valgrind --leak-check=full'
alias bc='bc -l'
alias cat="~/.scripts/cat.py"
alias pacman="$PACMAN"
alias myChronos=/data/Prog/mychronos_sh/myChronos.sh
alias df='dfc -a \
-p -rootfs,proc,sys,dev,run,devpts,shm,tmpfs,binfmt,gvfsd-fuse -d'
alias pacman='pacman-color'
alias gdb='gdb -q'
alias mmv='noglob zmv -W'
alias nautilus='nautilus --no-desktop'
alias zoid_mount='~/.scripts/sshfs.sh'
