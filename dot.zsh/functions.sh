man()
{
    env \
	LESS_TERMCAP_mb=$(printf "\e[1;35m") \
	LESS_TERMCAP_md=$(printf "\e[1;35m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;47;30m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;36m") \
	man "$@"
}

xtarrm()
{
    test x"$1" = x && echo 'usage: xtarcd <tarball>' >&2 && return 1
    local failed=0
    local ext=''
    case "$1" in
	*.tar.gz)  tar -xvzf "$1" || failed=$?
            ext='.tar.gz';;
	*.tgz)     tar -xvzf "$1" || failed=$?
            ext='.tgz';;
	*.tar.bz2) tar -xvjf "$1" || failed=$?
            ext='.tar.bz2';;
	*.tar.Z)   uncompress -c "$1" | tar -xf - || failed=$?
            ext='.tar.Z';;
	*.shar.gz) gunzip -c "$1" | unshar || failed=$?
            ext='.shar.gz';;
	*.zip)     unzip "$1" || failed=$?
            ext='.zip';;
	*.rar)     unrar e "$1" || failed=$?
            ext='.rar';;
	*)         echo 'Unknown file format' >&2; return 1;;
    esac
    test $failed -ne 0 && echo "extraction failed (returned $failed)" >&2 \
	&& return $failed
    rm -f "$1"
}

emacs_()
{
    EMACS_NO_X=foo emacs $* -nw
    unset EMACS_NO_X
}

diff()
{
    /usr/bin/diff -u $* | colordiff
}

cross_gnueabi()
{
    export ARCH=arm
    export CROSS_COMPILE=arm-none-linux-gnueabi-
}

cross_eabi()
{
    export ARCH=arm
    export CROSS_COMPILE=arm-none-eabi
}
