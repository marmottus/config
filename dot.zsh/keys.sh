bindkey -e
bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line
bindkey '^D'  delete-char             # Del
# bindkey '[3~' delete-char             # Del
bindkey '[[2~' overwrite-mode          # Insert
bindkey '[[5~' history-search-backward # PgUp
bindkey '[[6~' history-search-forward  # PgDn
