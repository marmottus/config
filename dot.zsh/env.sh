#######
# ENV #
#######

export NAME='Arthur Crépin-Leblond'
export FULLNAME="$NAME"
export EMAIL='arthur@marmottus.net'
export REPLYTO="$EMAIL"
export PAGER='less -r'
export TERM='rxvt-256color'

export LOGCHECK=60
export WATCHFMT="%n has %a %l from %M"
watch=(notme root)
export WATCH=all
export REPORTTIME=5

export NNTPSERVER="news.epita.fr"
export HISTORY=3000
export SAVEHIST=3000
export HISTFILE=$HOME/.history
export EDITOR='emacs'
export PREF_ADDR_EPITA="arthur.crepin-leblond"
export PACMAN='pacman-color'
export USECOLOR=1
export _JAVA_AWT_WM_NONREPARENTING=1;
export LESSOPEN="| src-hilite-lesspipe.sh %s"
export LESS=' -R '
export PATH="$PATH:/usr/bin/vendor_perl"
export PATH="$PATH:/usr/sbin"
export TZ='Europe/Paris'
export SHELL='/bin/zsh'

# ----- PROMPT -----
PROMPT='%{${fg_cyan}%}%n\
%(?..%{${fg_lgray}%}[%{${fg_red}%}%?%{${fg_lgray}%}])\
%{${fg_brown}%}\
%# %{${none}%}'

RPROMPT='%{${fg_brown}%}@%{${fg_blue}%}%M \
%{${fg_dgray}%}> %{${fg_red}%}%d\
%{${none}%}\
${vcs_info_msg_0_}\
%{${none}%}'

PS2=' %{${fg_purple}%}%_...%{${none}%}    '
PS3='?# '
PS4='+%N:%i:%_> '

[ -d /data/Prog/BSP/path/bin ] && \
    export PATH=$PATH:/data/Prog/BSP/path/bin
