########
# XSET #
########
[ -n "$DISPLAY" ] && [ `uname -s` != Darwin ] \
    && xset b off && xset r rate 250 75

zsh_hilight="/usr/share/zsh/plugins/\
zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
[ -f $zsh_hilight ] && source $zsh_hilight

umask 0022

if [ "x$DISPLAY" != x ]; then
    if [ `hostname` = "farnsworth" ]; then
	synclient TapButton2=3
	synclient TapButton3=2
	synclient HorizTwoFingerScroll=1
	synclient VertTwoFingerScroll=1
    fi

    if [ `hostname` = "heisenberg" ]; then
	synclient TouchPadOff=1
    fi
fi
