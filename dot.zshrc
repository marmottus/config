setopt prompt_subst

source ~/.zsh/colors.sh
source ~/.zsh/alias.sh
source ~/.zsh/functions.sh
source ~/.zsh/options.sh
source ~/.zsh/completion.sh
source ~/.zsh/keys.sh
source ~/.zsh/env.sh

source ~/.zsh/other.sh
