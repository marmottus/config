#! /bin/bash

while true; do
    ret=$(cat /tmp/.weechat_notify 2>/dev/null)
    if [ "${ret:-0}" = "1" ]; then
	client_irc=$(wmiir cat /tag/irc/ctl \
	    | grep 'select client' | awk '{ print $3 }')
	wmiir xwrite /client/${client_irc}/ctl Urgent on
	echo 0 > /tmp/.weechat_notify
	wmiir xwrite /client/${client_irc}/ctl Urgent off
    fi
    sleep 1;
done
