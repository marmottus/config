# -*- coding: utf-8 -*-
import re
from subprocess import Popen, PIPE
import math
import pygmi
from pygmi import *
from pygmi import event
from pygmi import monitor
from pygmi.monitor import monitors, Monitor

class Widgets:
    fun_number = 0
    def add(self, func):
        f = func(self)
        for i in range(len(f)):
            fun_name = name='fun_'+str(self.fun_number)
            if len(f[i]) == 2:
                monitors[fun_name] = Monitor(fun_name,
                                             action=(lambda self: f[i]))
            else:
                monitors[fun_name] = Monitor(fun_name,
                                             action=(lambda self: (None, f[i])))

            self.fun_number += 1

def volume():
    max_size = 10

    p = Popen(['amixer', '-c', '0', 'get', 'Master'], stdout=PIPE)
    out = p.communicate()[0]
    match = re.search('.+\[([0-9]+)%\].+\[(on|off)\]', out.decode())

    if match:
        value = int(match.group(1))
        state = match.group(2)
        if state == "off":
            buf = "Mute"
        else:
            buf = str(value) + '%'

        return wmii.cache['greencolor'], buf

    return ''

def ram():
    lines = open('/proc/meminfo', 'r').read().split('\n')

    used = None
    percent = None
    free = None
    mem = {}
    for line in lines:
        match = re.search('(.+): +([0-9]+).+', line)
        if match:
            key = match.group(1)
            value = int(match.group(2))
            if key == "MemTotal":   mem['total'] = math.floor(value/1024)
            if key == "MemFree":    mem['free'] = math.floor(value/1024)
            if key == "Buffers":    mem['buf'] = math.floor(value/1024)
            if key == "Cached":     mem['cached'] = math.floor(value/1024)


    if 'free' in mem and 'buf' in mem and 'cached' in mem:
        free = mem['free'] + mem['buf'] + mem['cached']
    if 'total' in mem and free:
        used = int(mem['total'] - free)
        percent = int(math.floor(used / mem['total'] * 100))

    return [(wmii.cache['greencolor'], percent),
            (wmii.cache['greencolor'], used)]


#print(volume())
