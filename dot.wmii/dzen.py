import time
from subprocess import Popen, PIPE
import re
import os
import sys
import math
import time
import random

class DZen():
    dzen_height = 22

    def color(self, c):
        return '^fg(' + c + ')'

    def backgroud(self, c):
        return '^bg(' + c + ')'

    def position(self, x='', y=''):
        return '^p('+str(x)+';'+str(y)+')'

    def position_abs(self, x='', y=''):
        return '^pa('+str(x)+';'+str(y)+')'

    def image(self, i):
        return self.position_abs(y=int(round(self.dzen_height/2))-8) \
            + '^i(' + os.getenv('HOME') + '/.wmii/icons/'\
            + i + ')'

    def circle(self, r):
        return self.position_abs(y=(int(round(self.dzen_height/2))\
                                        -int(round(r/2))))  \
                                        + '^c(' + str(r) + ')'

    def circle_outline(self, r):
        return self.position_abs(y=(int(round(self.dzen_height/2))\
                                        -int(round(r/2))))  \
                                        + '^co(' + str(r) + ')'

    def circle2(self, r, p):
        return self.position_abs(y=(int(round(self.dzen_height/2))\
                                   -int(round(r/2))))  \
                                   + '^c(' + str(r) + '-' + str(p) + ')'

    def rectangle(self, w, h):
        return '^r(' + str(w) + 'x' + str(h) + ')'

    def rectangle_outline(self, w, h):
        return '^ro(' + str(w) + 'x' + str(h) + ')'

    def color_value(self, v):
        if v < 30:
            return self.color('#88bb00')
        if v < 60:
            return self.color('#ffff00')
        if v < 80:
            return self.color('#ffaa00')
        return self.color('#ff0022')

    def ram_info(self):
        lines = open('/proc/meminfo', 'r').read().split('\n')

        used = None
        percent = None
        free = None
        total = None
        mem = {}
        for line in lines:
            match = re.search('(.+): +([0-9]+).+', line)
            if match:
                key = match.group(1)
                value = int(match.group(2))
                if key == "MemTotal":   mem['total'] = math.floor(value/1024)
                if key == "MemFree":    mem['free'] = math.floor(value/1024)
                if key == "Buffers":    mem['buf'] = math.floor(value/1024)
                if key == "Cached":     mem['cached'] = math.floor(value/1024)


                if 'free' in mem and 'buf' in mem and 'cached' in mem:
                    free = mem['free'] + mem['buf'] + mem['cached']
                    if 'total' in mem and free:
                        total = int(mem['total'])
                        used = int(total - free)
                        percent = int(math.floor(used / total * 100))

        return (percent, used, total)

    def ram(self):
        circle_radius=int(round(self.dzen_height*0.9))
        percent, used, total = self.ram_info()
        phi = int(round((percent*180)/50))

        return self.position(x=0) + self.color('#cccccc') \
            + self.image('mem.xbm') + '^ib(1)' + self.position(x=5) \
            + self.color('#666666') \
            + self.circle_outline(circle_radius)\
            + self.position(x=-circle_radius)\
            + self.color('#88bb00') + self.circle(circle_radius)\
            + self.position(x=-circle_radius) + self.color('#880022') \
            + self.circle2(circle_radius, phi) \
            + self.position_abs(y=round(self.dzen_height/2) - 7)\
            + self.position(x=3)\
            + self.color_value(int(round((used*100)/total))) \
            + str(used) +' Mio ^ib(0)'

    def volume_info(self):
        p = Popen(['amixer', '-c', '0', 'get', 'Master'], stdout=PIPE)
        out = p.communicate()[0]
        match = re.search('.+\[([0-9]+)%\].+\[(on|off)\]', out.decode())

        if match:
            value = int(match.group(1))
            state = match.group(2)
            return (value, state)

        return (None, None)

    def volume(self):
        max_blocks = 10
        offset = 2
        max_height = round((self.dzen_height-offset) * 0.9)
        block_width = 3
        block_color = '#0088bb'
        no_block_color = '#9999aa'

        value, state = self.volume_info()

        if state == "off":
            return self.position_abs(y=2) + self.image('vol-mute.xbm') + \
                '^p(+10)' + self.color('#ff0022') + 'Mute '

        buf = ' ' + self.image('vol-hi.xbm') + '^p(+10)'
        nblocks = int(round((value*max_blocks)/100))
        for i in range(1, nblocks+1):
            height = int(round((i*max_height)/max_blocks))
            buf += self.color(block_color)\
                + self.position_abs(y=self.dzen_height-height-offset)\
                + self.rectangle(block_width, height) + '^p(+2)'
        for i in range(nblocks+1, max_blocks):
            height = int(round((i*max_height)/max_blocks))
            buf += self.color(no_block_color)\
                + self.position_abs(y=self.dzen_height-height-offset)\
                + self.rectangle(block_width, height) + '^p(+2)'
        return buf + ' '

    cpu_width = 20
    graph = [0 for i in range(cpu_width)]

    def cpu(self):
        offset = 2
        max_height = round((self.dzen_height-offset) * 0.9)
        block_width = 3
        bg_width = (block_width+1)*self.cpu_width

        cpu_value = self.cpu_info(0.5)
        for i in range(len(self.graph)-1):
            self.graph[i] = self.graph[i+1]
            self.graph[-1] = cpu_value

        buf = '^ib(1)' + self.color('#cccccc') + self.image('cpu.xbm') \
            + self.position(x=5)
        buf += self.color('#002233') \
            + self.position_abs(y=0)\
            + self.rectangle(bg_width, self.dzen_height) \
            +  self.color_value(self.graph[-1]) + self.position(x=-bg_width)
        j=0

        # height = int(round((20*max_height)/100))
        # print(buf)
        for v in self.graph:
            buf += self.position(x=1)
            height = int(round((v*max_height)/100))
            for i in range(int(height/2)):
                buf += self.position_abs(y=self.dzen_height-offset-i*2)\
                    + self.rectangle(block_width, 1)\
                    + self.position(x=-block_width)
            buf += self.position(x=block_width)

# #
# #            for i in range(5):
# #                for k in range(height):
#             buf += self.position_abs(y=self.dzen_height-height-offset)
#             buf += self.rectangle(3, 1)

#             buf += self.position(x=3)
#             j += 1
        return buf

    def getTimeList(self):
        statFile = open("/proc/stat", "r")
        timeList = statFile.readline().split(" ")[2:6]
        statFile.close()
        for i in range(len(timeList)):
            timeList[i] = int(timeList[i])
        return timeList

    def deltaTime(self,interval):
        x = self.getTimeList()
        time.sleep(interval)
        y = self.getTimeList()
        for i in range(len(x)):
            y[i] -= x[i]
        return y

    def cpu_info(self, interval):
        dt = self.deltaTime(interval)
        cpuPct = 100 - (dt[len(dt) - 1] * 100.00 / sum(dt))
        return cpuPct

    def brightness_info(self):
        try:
            actualb = open('/sys/class/backlight/'\
                               + 'acpi_video0/actual_brightness', 'r').read()
            maxb = open('/sys/class/backlight/'\
                            + 'acpi_video0/max_brightness', 'r').read()

            return int(actualb), int(maxb)
        except:
            return -1,-1

    def brightness(self):
        max_blocks = 10
        offset = 5
        height = round((self.dzen_height-offset) * 0.5)
        block_width = 5
        block_color = '#0088bb'
        no_block_color = '#9999aa'

        actualb, maxb = self.brightness_info()

        buf = self.color('#cccccc') \
        + ' ' +  self.image('bulb.xbm') + '^p(+5)'
        x_off = (block_width + 2)*max_blocks
        buf += self.position_abs(y=round(self.dzen_height/2)-height/2)\
            + self.rectangle_outline(x_off+5, height*1.5)

        nblocks = int(round((actualb*max_blocks)/maxb))
        buf += self.position(x=-x_off-2)
        for i in range(1, nblocks+1):
            buf += self.color(block_color)\
                + self.position_abs(y=self.dzen_height-height-offset)\
                + self.rectangle(block_width, height) \
                + self.position(x=2)

        return buf + self.position(x=1)


    def battery_info(self):
        p = Popen(['acpi', '-a'], stdout=PIPE)
        out = p.communicate()[0]
        match = re.search('([0-9]+)%', out.decode())
        if match:
            return int(match.group(1))
        else:
            return 0

    def battery(self):
        max_blocks = 10
        offset = 5
        height = round((self.dzen_height-offset) * 0.5)
        block_width = 5
        block_color = '#0088bb'
        no_block_color = '#9999aa'

        value = self.battery_info()

        buf = self.color('#cccccc')\
            + ' ' +  self.image('power-bat.xbm') + '^p(+5)'

        x_off = (block_width + 2)*max_blocks
        buf += self.position_abs(y=round(self.dzen_height/2)-height/2)\
            + self.rectangle_outline(x_off+5, height*1.5)

        nblocks = int(round((value*max_blocks)/100))
        buf += self.position(x=-x_off-2)
        for i in range(1, nblocks+1):
            buf += self.color_value(100-value)\
                + self.position_abs(y=self.dzen_height-height-offset)\
                + self.rectangle(block_width, height) \
                + self.position(x=2)

        return buf + self.position(x=x_off)

    widgets_list = [volume, ram, cpu, battery, brightness]

    screen_width = 1366
    def run(self):
        p = Popen([os.getenv('HOME') + '/.scripts/one_instance.sh',
                   'dzen2','-h', str(self.dzen_height),
                   '-ta', 'l',
                   '-dock',
#                   '-y', '2',
                   '-w', sys.argv[1],
                   # '-x', str(1440/3),
                   '-bg', '#003850',
                   '-fn', 'Monospace-10'],
                  stdin=PIPE)
        while True:
            for w in self.widgets_list:
                p.stdin.write((w(self) + '   ').encode('UTF-8'))

            p.stdin.write('\n'.encode('UTF-8'))
            time.sleep(1)
d =DZen()
d.run()
