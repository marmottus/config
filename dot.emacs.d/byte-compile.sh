#! /bin/bash

if [ $# -ge 1 ]; then
    files="$*"
else
    files=$(find -name '*.el')
fi

for f in $files; do
    echo $f " -> " ${f}c
    emacs -batch -f batch-byte-compile $f >&2 2>/dev/null
done
