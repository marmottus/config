;; ----------------------------------------------------------------------------
;; Configs for X

(add-to-list 'load-path "~/.emacs.d/themes")
(add-to-list 'custom-theme-load-path
	     "~/.emacs.d/themes/emacs-color-theme-solarized")

(defun config-x()

  ;; Highlight current line
  (require 'highlight-current-line)
  (highlight-current-line-on t)
  (set-face-background 'highlight-current-line-face "#001025")

  ;; Keybindings
  (global-set-key [C-S-insert] 'x-clipboard-yank)
  (global-set-key [(control z)] 'undo)
  (global-set-key [C-S-z] 'redo)
  (setq x-select-enable-clipboard t)

  ;; Color theme
  ;; (load "dirac-x.elc")
  ;; (require 'color-theme)
  ;; (color-theme-initialize)
  ;; (your-config-name-here)

  ;; Custom
  (custom-set-faces
   '(default ((t (:stipple nil
                 :background "#001018"
                 :foreground "#cccccc"
                 :height 110
                 :family "Monospace"))))))

;; (load "color-theme-almost-monokai.elc")
;; (require 'color-theme)
;; (color-theme-initialize)
(load-theme 'solarized-dark t)
;;(color-theme-almost-monokai)


;; (defun config-no-x()
  ;; (require 'color-theme)
  ;; (load "dirac-no-x.elc")
  ;; (color-theme-dirac))

(if (getenv "DISPLAY")                ;; If DISPLAY is set
    (if (not (getenv "EMACS_NO_X"))   ;; If EMACS_NO_X is not set
        (config-x)))
  ;;     (config-no-x))
  ;; (config-no-x))


;; ----------------------------------------------------------------------------
;; Delete trailing whitespaces when saving.

(defun delete-trailing-whitespace-exit ()
  (if (string= ".article" (buffer-name))
      ()
    (if (string= ".followup" (buffer-name))
    ()
      (delete-trailing-whitespace))))

(add-hook 'write-file-hooks 'delete-trailing-whitespace-exit)

;; ----------------------------------------------------------------------------
;; Higlight lines > 80 columns

(defun eightycols nil
  (defface line-overflow
    '((t (:background "red" :foreground "white")))
    "Face to use for `hl-line-face'.")
  (highlight-regexp "^.\\{80,\\}$" 'line-overflow))

(add-hook 'find-file-hook 'eightycols)

;; ----------------------------------------------------------------------------
;; Insert header guard in C/C++ header file
;; the recognized extensions are .h, .hh or .hxx

(defun insert-header-guard ()
  (interactive)
  (if (string-match "\\.h\\(h\\|xx\\)?$" (buffer-name))
      (let ((header-guard
             (upcase (replace-regexp-in-string "[-.]" "_" (buffer-name)))))
        (save-excursion
          (goto-char (point-min))
          (insert "#ifndef " header-guard "_\n")
          (insert "# define " header-guard "_\n\n")
          (goto-char (point-max))
          (insert "\n#endif /* !" header-guard "_ */")))
    (message "Invalid C/C++ header file.")))
