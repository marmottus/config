;;
;; .emacs for emacs configuration in /home/arthur
;;
;; Made by Arthur Crépin-Leblond
;; Login   <arthur.crepin-leblond@epita.fr>
;;
;; Started on  Tue Jan 11 19:20:58 2011 Arthur Crépin-Leblond
;; Last update Wed Mar 16 12:03:04 2011 Arthur Crépin-Leblond
;;
;; ----------------------------------------------------------------------------
;; Set extern load path
(add-to-list 'load-path "~/.emacs.d")
(add-to-list 'load-path "~/.emacs.d/ac")
(add-to-list 'load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/others")

;; ----------------------------------------------------------------------------
;; Disable VC support
(setq vc-handled-backends nil)
;; ----------------------------------------------------------------------------
;; Show lines on left
(require 'linum+)
(global-linum-mode 1)

;; ----------------------------------------------------------------------------
;; ASM slash comments
;; (custom-set-variables
;;  '(asm-comment-char "/"))

;; ----------------------------------------------------------------------------
;; Markdown
(autoload 'markdown-mode "markdown-mode.el"
  "Major mode for editing Markdown files" t)

(setq auto-mode-alist
   (cons '("\\.md" . markdown-mode) auto-mode-alist))
(setq auto-mode-alist
   (cons '("\\.markdown" . markdown-mode) auto-mode-alist))

;; ----------------------------------------------------------------------------
;; PHP
(autoload 'php-mode "php-mode.el" "Php mode." t)
(setq auto-mode-alist (append '(("/*.\.php[345]?$"
				 . php-mode)) auto-mode-alist))

;; ----------------------------------------------------------------------------
;; Yasnippet
(require 'yasnippet-bundle)

;; ----------------------------------------------------------------------------
;; Auto complete

(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac/ac-dict")
(ac-config-default)

;; ----------------------------------------------------------------------------
;; BSD indentation mode
(setq c-default-style "bsd" c-basic-offset 2)

(setq tab-width 4)
(setq indent-tabs-mode nil)

;; ----------------------------------------------------------------------------
;; Show tabs
;; (defface extra-whitespace-face
;;   '((t (:background "pale green")))
;;   "Used for tabs and such.")

;(standard-display-ascii ?\t "····")

;; (defvar my-extra-keywords
;;   '(("\t" . 'extra-whitespace-face)))

;; (add-hook 'makefile-mode-hook
;; 	  (lambda () (font-lock-add-keywords nil my-extra-keywords)))

;; ----------------------------------------------------------------------------
;; Erase ~ files
(setq make-backup-files nil)

;; ----------------------------------------------------------------------------
;; CMake
;; (require 'cmake-mode)
(setq auto-mode-alist (cons '("CMakeLists.txt$" . cmake-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\.cmake$" . cmake-mode) auto-mode-alist))
(autoload 'cmake-mode "cmake-mode" "CMake editing mode." t)

;; ----------------------------------------------------------------------------
;; Lua mode
(setq auto-mode-alist (cons '("\.lua$" . lua-mode) auto-mode-alist))
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)

;; ----------------------------------------------------------------------------
;; PKGBUILD mode
(autoload 'pkgbuild-mode "pkgbuild-mode" "PKGBUILD editing mode." t)
(setq auto-mode-alist (cons '("PKGBUILD$" . pkgbuild-mode) auto-mode-alist))

;; ----------------------------------------------------------------------------
;; Tuareg Mode
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(setq auto-mode-alist
(cons '("\\.ml[iylp]?\\'" . tuareg-mode) auto-mode-alist))

;; ----------------------------------------------------------------------------
;; Allow post-fix accents.
(setq default-input-method "french-postfix")

;; ----------------------------------------------------------------------------
;; Auto fill mode with 80 columns.
(setq fill-column 80)
(add-hook 'c-mode-common-hook
	  (lambda () (auto-fill-mode)))

;; ----------------------------------------------------------------------------
;; No left scrollbar
(set-scroll-bar-mode nil)

;; ----------------------------------------------------------------------------
;; Minimal interface (because we use emacs with keyboard. not mouse !!!)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; ----------------------------------------------------------------------------
;; Skip message a startup
(setq inhibit-startup-message t)

;; ----------------------------------------------------------------------------
;; Empty stratch buffer
(setq initial-scratch-message nil)

;; ----------------------------------------------------------------------------
;; Show matching parentheses
(show-paren-mode t)

;; ----------------------------------------------------------------------------
;; Enable syntaxic highlighting
(global-font-lock-mode t)

;; ----------------------------------------------------------------------------
;; Show line/column number in modeline
(column-number-mode t)
(line-number-mode t)

;; ----------------------------------------------------------------------------
;; Highlight trailing whitespace
(setq-default show-trailing-whitespace t)

;; ----------------------------------------------------------------------------
;; URxvt support
(load "rxvt.elc")

;; ----------------------------------------------------------------------------
;; Nice keyboard bindings to move between windows

(global-set-key [M-left] 'windmove-left)
(global-set-key [M-right] 'windmove-right)
(global-set-key [M-up] 'windmove-up)
(global-set-key [M-down] 'windmove-down)

(global-set-key (kbd "ESC <up>") 'windmove-up)
(global-set-key (kbd "ESC <down>") 'windmove-down)
(global-set-key (kbd "ESC <left>") 'windmove-left)
(global-set-key (kbd "ESC <right>") 'windmove-right)

;; ----------------------------------------------------------------------------
;; Keyboard bindings for resizing windows.

(global-set-key [M-S-left] 'shrink-window-horizontally)
(global-set-key [M-S-right] 'enlarge-window-horizontally)
(global-set-key [M-S-down] 'enlarge-window)
(global-set-key [M-S-up] 'shrink-window)

(global-set-key (kbd "ESC <S-up>") 'shrink-window)
(global-set-key (kbd "ESC <S-down>") 'enlarge-window)
(global-set-key (kbd "ESC <S-left>") 'shrink-window-horizontally)
(global-set-key (kbd "ESC <S-right>") 'enlarge-window-horizontally)

;; ----------------------------------------------------------------------------
;; Configure ido-mode
;; advanced completion in minibuffer

;; enable ido-mode
(ido-mode t)

;; do not search files in other directories
(setq ido-auto-merge-work-directories-length -1)

;; ----------------------------------------------------------------------------
;; Configure compilation in Emacs

;; scroll compilation buffer to follow compilation
(setq compilation-scroll-output t)

;; bind recompile to C-c c
(global-set-key [(control c) (c)] 'compile)

;; bind next-error to C-c e
(global-set-key [(control c) (e)] 'next-error)

(load "functions.elc")
