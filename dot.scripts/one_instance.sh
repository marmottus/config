#!/bin/bash

ps aux | awk '{ print $11 }' | grep $1 >/dev/null
if ! [ $? -eq 0 ]; then
    exec $* &
fi
