#! /bin/bash

old_date=$(cat .old_date)
current_date=$(date '+%s')

diff_date=$((current_date - old_date))
nb_days=$(php -r "date_default_timezone_set('Europe/Paris'); echo date('j', $diff_date);")

last_date=$(php -r "date_default_timezone_set('Europe/Paris');								     \
                        echo date('d/m/Y G:i:s', time () - $diff_date);							     \
                        echo (' => ');											     \
                        echo date('j \d\a\y(\s), G \h\o\u\r(\s), i \m\i\i\n\u\t\e(\s), s \s\e\c\o\n\d(\s)', $diff_date);")

if [ $nb_days -gt 3 ]; then
    echo "You don't have updated your system since $last_date"
    echo "Updating ArchLinux..."
    yaourt -Syy && echo $current_date > .old_date;
else
    echo "Last update: $last_date"
fi;
