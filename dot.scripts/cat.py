#! /usr/bin/env python2
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE
import sys
import re
import os

cat_small_opts = ['A', 'b', 'e', 'E', 'n', 's', 't', 'T', 'u', 'v']
cat_long_opts = ['show-all', 'number-nonblank', 'show-ends', 'number',
                 'squeeze-blank', 'show-tabs', 'show-nonprinting',
                 'help', 'version']

cat_args = ['/usr/bin/cat']
src_hilite = ['/usr/bin/src-hilite-lesspipe.sh']
def error(arg):
    print("cat: invalid option -- '" + arg + "'")
    print("Try 'cat --help' for more information.")
    sys.exit(1)

for arg in sys.argv[1:]:
    match_small = re.search('^-([a-zA-Z])$', arg)
    if match_small:
        if match_small.group(1) in cat_small_opts:
            cat_args.append('-' + match_small.group(1))
        else:
            error(match_small.group(1))

    match_long = re.search('^--([a-zA-Z\-]+)$', arg)
    if match_long:
        if match_long.group(1) in cat_long_opts:
            cat_args.append('--' + match_long.group(1))
        else:
            error(match_long.group(1))

    if not match_long and not match_small:
        src_hilite.append(arg)

if len(src_hilite) == 1:
    os.execv(cat_args[0], cat_args)
else:
    p = Popen(src_hilite, stdout=PIPE)
    stdout = p.communicate()
    p = Popen(cat_args, stdin=PIPE)
    p.stdin.write(stdout[0]) #.encode('UTF-8'))
