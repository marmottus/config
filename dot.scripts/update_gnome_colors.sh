# /bin/bash

for f in /usr/share/themes/Adwaita/{gtk-2.0/gtkrc,gtk-3.0/settings.ini}
do
    sudo sed -r 's/(base_color):#[a-zA-Z0-9]{6}/\1:#FFFFFF/' $f -i
    sudo sed -r 's/(fg_color):#[a-zA-Z0-9]{6}/\1:#000000/' $f -i
    sudo sed -r 's/(tooltip_fg_color):#[a-zA-Z0-9]{6}/\1:#000000/' $f -i
    sudo sed -r 's/(selected_bg_color):#[a-zA-Z0-9]{6}/\1:#002A5A/' $f -i
    sudo sed -r 's/(selected_fr_color):#[a-zA-Z0-9]{6}/\1:#FFFFFF/' $f -i
    sudo sed -r 's/(text_color):#[a-zA-Z0-9]{6}/\1:#1A1A1A/' $f -i
    sudo sed -r 's/(tooltip_bg_color):#[a-zA-Z0-9]{6}/\1:#ECE196/' $f -i

    echo Updating $f...
done
