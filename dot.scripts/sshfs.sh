found=$(ls -A /media/zoidberg/ 2>/dev/null)
#addr=zoidberg.marmottus.net
addr=192.168.0.11

if [ "$found" = "" ]; then
    sshfs arthur@${addr}:/data /media/zoidberg/
fi
