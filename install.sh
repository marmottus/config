#! /bin/sh

remove()
{
    if [ -e $1 ]; then
	echo "removing $1..."
	rm -rf $1
    fi
}

symlink()
{
    echo "installing $2"
    ln -s $PWD/$1 $2
}

FILES=".hgrc .gitconfig .zsh .zshrc .scripts .emacs .emacs.d .wmii .slrnrc\
 .Xdefaults .Xresources .urxvt .weechat .ls++.conf .screenrc"

echo "/!\\ Following files will be erased /!\\"
for f in $FILES; do
    echo $HOME/$f
done
echo -n "Do you want to continue? [N/y] "

read input

if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
    for f in $FILES; do
	remove $HOME/$f
	symlink dot$f $HOME/$f
    done

    mkdir -p $HOME/.news && touch $HOME/.news/score
    xrdb -load $HOME/.Xdefaults
fi
